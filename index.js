// Find users with letters

db.users.find({$or : [{firstName: "s"}, 
	{lastName: "d"}]
	},
	{
		firstName: 1,
		lastName: 1,
		_id:0
	});
// Find users from HR

db.users.find({$and: [{department: "HR"}, {age: {$gte: 70}}]});

// find users with letter 'e' and lte 30

db.users.find({$and : [{firstName: {$regex : "e", $options: '$1'}}, {age: {$lte : 30}}]});